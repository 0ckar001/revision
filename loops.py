import math
# 1
print("Zadane 1:")
lista = [12, 10, 11, 9, 8, 6, 13, 16, 17, 14, 10, 5]
def foo1(lista, k):
    for i in range(len(lista)):
        for j in range(i + 1, len(lista)):
            if k == lista[i] + lista[j]:
                return True
    return False


print(foo1(lista, int(input("Give a number: "))))

# 2
def foo2(string1, string2):
    out = ""
    for i in range(len(string1)):
        if string1[i] == string2[i]:
            out += string1[i]
        else:
            break
    return out


print(foo2("abcdefg", "abcefg"))
# 3
def foo3(lista):
    min = math.inf
    max = -math.inf
    for i in range(len(lista) - 1, -1 ,-1):
        for j in range(len(lista) - (len(lista) - i + 1), -1, -1):
            if i > max:
                max = i
            if j < min and j < max:
                min = j
    return max - min


print(foo3(lista))
