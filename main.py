# 1
print("zadanie 1:")
def foo1(n):
    if n < 17:
        return -2 * (n - 17)
    else:
        return n - 17

print(foo1(int(input("Give a number: "))))
# 2
print("zadanie 2:")
def foo2(n):
    if n == 1:
        return 1
    elif n == 2:
        return 2
    elif n == 3:
        return 6
    elif n == 4:
        return 4
    else:
        return 0

print(foo1(int(input("Give a number: "))))
# 3
print("zadanie 3:")
def foo3(n):
    return n // 5 + n // 25 + n // 125 + n // 625 + n // 3125 + n // 15625

# testowanie
import math
for _ in range(600, 700):
    print(math.factorial(_), "-", foo3(_), ",", _)
