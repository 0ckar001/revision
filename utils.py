def getDigits(n):
    digits = []
    while n != 0:
        digits.insert(0, n % 10)
        n //= 10
    return digits