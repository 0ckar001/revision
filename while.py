import math
import utils

def func(x):
    return x * 2 - 4


print("Zadanie 0:")
def zeroOfFunction(f):
    x1 = 10000000
    x2 = -10000000
    m = 0
    while f(m) != 0:
        if f(x1) * f(m) > 0:
            x1 = m
        else:
            x2 = m
        m = (x1 + x2)/2
    return m

print(zeroOfFunction(func))

print("Zadanie 1:")
def foo1(x):
    iters = 0
    if x <= 0:
        return 0
    else:
        while x > 0:
            x -= sum(utils.getDigits(x))
            iters += 1
        return iters

print(foo1(int(input("Give a number: "))))

print("Zadanie 2:")
def foo2(x, depth=65536):
    while x != 1:
        x = sum([_ ** 2 for _ in utils.getDigits(x)])
        depth -= 1
        if depth <= 0:
            return False
    return True

print(foo2(int(input("Give a number: "))))